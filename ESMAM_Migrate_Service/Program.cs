﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ESMAM_Migrate_Service
{
    class Program
    {
        static void Main()
        {
            //程序意外停止 事件，捕获其异常原因
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            { //服务自动执行代码：

                //ServiceBase[] ServicesToRun;
                //ServicesToRun = new ServiceBase[]
                //{
                //    new Migrate_Service()
                //};
                //ServiceBase.Run(ServicesToRun);
            }


            {//手动执行代码(如使用服务执行，则注释掉该代码)：

                Console.WriteLine("ESMAM迁移服务 开始执行：" + DateTime.Now);
                Stopwatch wct = new Stopwatch();
                wct.Restart();

                ServiceUtil.Exec();

                wct.Stop();

                Console.ReadKey();
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogUtils.LogWarn("ESMAM迁移服务 出错", Developer.MJ, e.ExceptionObject, e.IsTerminating);
        }
    }
}
