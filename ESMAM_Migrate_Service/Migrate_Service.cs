﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ESMAM_Migrate_Service
{
    partial class Migrate_Service : ServiceBase
    {
        public Migrate_Service()
        {
            this.ServiceName = "xxxx服务";
        }

       
        public static Timer timer = new Timer();
        public static double interval = (1000 * 60) * 1; // 1 分钟 间隔执行
        /// <summary>
        /// 如果 间隔执行设定修改，则在下次执行更新间隔时间段
        /// </summary>
        private void RefashInterval()
        {
            ConfigurationManager.RefreshSection("connectionStrings");
            ConfigurationManager.RefreshSection("appSettings");
            interval = Utils.ConvertTo<double>(ConfigurationManager.AppSettings["Interval"], 1);
            timer.Interval = interval * (1000 * 60);

        }

        protected override void OnStart(string[] args)
        {
            RefashInterval();
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();
            LogUtils.LogInfo(ServiceName + " 启动", Developer.SysDefault);
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            RefashInterval();
            try
            {
                ServiceUtil.Exec();
            }
            catch (Exception ex)
            {
                LogUtils.LogError(ServiceName + "  异常", Developer.SysDefault, ex);
            }
        }

        protected override void OnStop()
        {
            LogUtils.LogInfo(ServiceName + " 停止", Developer.SysDefault);
        }

        protected override void OnShutdown()
        {
            LogUtils.LogInfo(ServiceName + " 关闭", Developer.SysDefault);
        }
    }
}
